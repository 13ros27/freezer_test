"""Reads the temperature of a given DS18B20 sensor."""
from glob import glob
from os import listdir, path, system
from time import sleep

system('modprobe w1-gpio')
system('modprobe w1-therm')


class Sensor:
    """Holds information about the sensor."""

    def __init__(self, code, retry=-1):
        """Find the path to the given sensor."""
        while True:
            self.position = '/sys/bus/w1/devices/' + code + '/w1_slave'
            if path.isfile(self.position):
                self.sensor = glob(self.position)[0]
            else:
                if retry != -1:
                    sleep(retry)
                    continue
                else:
                    self.sensor = None
            break

    @staticmethod
    def find_sensors():
        """Return a list of all connected sensors."""
        possible_sensors = listdir('/sys/bus/w1/devices')
        sensors = []
        for code in possible_sensors:
            if path.isfile('/sys/bus/w1/devices/' + code + '/w1_slave'):
                sensors.append(code)
        return sensors

    def _read_file(self):
        try:
            if self.sensor is not None:
                with open(self.sensor, 'r') as fp:
                    lines = fp.readlines()
                return lines
            else:
                return None
        except IOError:
            return None

    @property
    def temp(self):
        """Read the temperature of the sensor or None."""
        lines = self._read_file()
        if lines is not None:
            if lines[1].find('YES') is False:
                temp_c = None
            else:
                while lines[0].strip()[-3:] != 'YES':
                    lines = self._read_file()
                equal_pos = lines[1].find('t=')
                if equal_pos != -1:
                    try:
                        temp_c = int(lines[1][equal_pos+2:-2])/100.0
                    except ValueError:
                        temp_c = None
        else:
            temp_c = None
        return temp_c
