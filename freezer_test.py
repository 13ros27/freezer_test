"""Logs the freezer temperature every minute (approximately)."""
import os
import RPi.GPIO as GPIO
import temperature
import time

SENSOR_ID = '28-000433591bff'
LED_PIN = 13

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(LED_PIN, GPIO.OUT)

path = os.path.dirname(os.path.abspath(__file__))
i = 0
while True:
    if not os.path.exists(path + '/' + str(i)):
        path += '/' + str(i)
        os.makedirs(path)
        break
    else:
        i += 1

sensor = temperature.Sensor(SENSOR_ID, retry=1)
with open(path + '/log1.csv', 'w') as f:
    f.write('Time, Temperature\n')
with open(path + '/log2.csv', 'w') as f:
    f.write('Time, Temperature\n')

while True:
    temp = sensor.temp
    cur_time = time.strftime('%H:%M:%S')
    with open(path + '/log1.csv', 'a') as fp:
        fp.write(cur_time + ', ' + str(temp) + '\n')
    with open(path + '/log2.csv', 'a') as fp:
        fp.write(cur_time + ', ' + str(temp) + '\n')
    if temp is not None:
        GPIO.output(LED_PIN, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(LED_PIN, GPIO.LOW)
    else:
        time.sleep(1)
    time.sleep(58)
